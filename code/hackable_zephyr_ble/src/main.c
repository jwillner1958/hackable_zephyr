/*
 *
 *  hackable_zephyr_ble
 * 
 * 	main.c
 * 
 * 	Demonstrates usin Electronut Labs hackaBLE Nordic nRF52832 dev board 
 *  with Zephy RTOS.
 * 
 * 	This example is adapted from Zephr RTOS sensor and bluetooth examples.
 * 
 * 	Mahesh Venkitachalam
 * 	electronut.in
 * 
 */

#include <zephyr.h>
#include <misc/printk.h>
#include <device.h>
#include <gpio.h>

#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <misc/byteorder.h>
#include <sensor.h>

#include <settings/settings.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>


// sensor
static struct device* dev_bme280;

/* Custom Service Variables */
static struct bt_uuid_128 vnd_uuid = BT_UUID_INIT_128(
	0xf0, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);


static const struct bt_uuid_128 T_uuid = BT_UUID_INIT_128(
	0xf1, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x13,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x13);


static const struct bt_uuid_128 H_uuid = BT_UUID_INIT_128(
	0xf2, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x13,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x13);

static struct bt_gatt_ccc_cfg T_ccc_cfg[BT_GATT_CCC_MAX];
static struct bt_gatt_ccc_cfg H_ccc_cfg[BT_GATT_CCC_MAX];

static u8_t bTNotify;
static u8_t bHNotify;

static void T_ccc_cfg_changed(const struct bt_gatt_attr *attr, u16_t value)
{
	bTNotify = (value == BT_GATT_CCC_NOTIFY) ? 1 : 0;
}

static void H_ccc_cfg_changed(const struct bt_gatt_attr *attr, u16_t value)
{
	bHNotify = (value == BT_GATT_CCC_NOTIFY) ? 1 : 0;
}
// read temperature
static s32_t T_vals[] = {0, 0};
static ssize_t read_T(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, u16_t len, u16_t offset)
{
	const char *value = attr->user_data;

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(T_vals));
}	

// read humidity
static s32_t H_vals[] = {0, 0};
static ssize_t read_H(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			   void *buf, u16_t len, u16_t offset)
{
	const char *value = attr->user_data;

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 sizeof(H_vals));
}	


/* Vendor Primary Service Declaration */
static struct bt_gatt_attr vnd_attrs[] = {
	/* Vendor Primary Service Declaration */
	BT_GATT_PRIMARY_SERVICE(&vnd_uuid),
	BT_GATT_CHARACTERISTIC(&T_uuid.uuid, 
					BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
			       	BT_GATT_PERM_READ,
			       	read_T, NULL, T_vals),
    BT_GATT_CCC(T_ccc_cfg, T_ccc_cfg_changed),
    BT_GATT_CHARACTERISTIC(&H_uuid.uuid, 
					BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
			       	BT_GATT_PERM_READ,
			       	read_H, NULL, H_vals),
	BT_GATT_CCC(H_ccc_cfg, H_ccc_cfg_changed)
};

static struct bt_gatt_service vnd_svc = BT_GATT_SERVICE(vnd_attrs);

static volatile u8_t mfg_data[] = { 0x00, 0x00, 0xaa, 0xbb };

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
	BT_DATA(BT_DATA_MANUFACTURER_DATA, mfg_data, 4),

	BT_DATA_BYTES(BT_DATA_UUID128_ALL,
		      0xf0, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
		      0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12),
};

static void connected(struct bt_conn *conn, u8_t err)
{
	if (err) {
		printk("Connection failed (err %u)\n", err);
	} else {
		printk("Connected\n");
	}
}

static void disconnected(struct bt_conn *conn, u8_t reason)
{
	printk("Disconnected (reason %u)\n", reason);
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

static void bt_ready(int err)
{
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");

	bt_gatt_service_register(&vnd_svc);

	if (IS_ENABLED(CONFIG_SETTINGS)) {
		settings_load();
	}

	err = bt_le_adv_start(BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE(ad), NULL, 0);
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Advertising successfully started\n");
}

// print BME280 data
void update_sensor_data()
{

    // get sensor data
    struct sensor_value temp, press, humidity;

    sensor_sample_fetch(dev_bme280);
    sensor_channel_get(dev_bme280, SENSOR_CHAN_AMBIENT_TEMP, &temp);	
    sensor_channel_get(dev_bme280, SENSOR_CHAN_PRESS, &press);
    sensor_channel_get(dev_bme280, SENSOR_CHAN_HUMIDITY, &humidity);

    char strData[64];
    sprintf(strData, "T: %d H: %d",
            temp.val1, 
            humidity.val1);
    printk("temp: %d.%06d; press: %d.%06d; humidity: %d.%06d\n",
            temp.val1, temp.val2, press.val1, press.val2,
            humidity.val1, humidity.val2);

	mfg_data[2] = (uint8_t)temp.val1;
	mfg_data[3] = (uint8_t)humidity.val1;

	T_vals[0] = temp.val1;
    T_vals[1] = temp.val2;
    H_vals[0] = humidity.val1;
    H_vals[1] = humidity.val2;
}


void main(void)
{
	struct device* port0 = device_get_binding("GPIO_0");
	/* Set LED pin as output */
    gpio_pin_configure(port0, 17, GPIO_DIR_OUT);

	// flash  LED
	gpio_pin_write(port0, 17, 0);
	k_sleep(500);
	gpio_pin_write(port0, 17, 1);
	k_sleep(500);

	// sensor
	dev_bme280 = device_get_binding("BME280");
	printk("dev %p name %s\n", dev_bme280, dev_bme280->config->name);

	k_sleep(500);
	update_sensor_data();

    // set up BLE
	int err;
	err = bt_enable(bt_ready);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	bt_conn_cb_register(&conn_callbacks);
	
	while (1) {
		k_sleep(2*MSEC_PER_SEC);

		// update 
		update_sensor_data();
		
		// notify 
		bt_gatt_notify(NULL, &vnd_attrs[2], T_vals, sizeof(T_vals));
        bt_gatt_notify(NULL, &vnd_attrs[4], H_vals, sizeof(H_vals));

		// update adv data
		bt_le_adv_update_data(ad, ARRAY_SIZE(ad), NULL, 0);
	}
}

